Twitter and natural language processing in Python
=================================================


This repository implements the collection, manipulation and analysis of twitter text data in Python mainly using time serie analysis and natural language processing techniques in python.
The implementation includes

1. Methods for collection, archive and management of twitter data; defaulted to download a daily tweet file related to *Brexit*
2. Methods for analysis of twitter data; specifically regarding natural language processing, topic modelling, extraction of topics and sentiment analysis
3. Example of usage in a Python notebook and its export in an html format


|


Getting twitter data
--------------------

The first module *tweets.py* implements method for the collection, archive and management of twitter data. 
Methods includes 

1) Method to automatically download and save yesterday's tweet into a file
2) Method to download all history available for a twitter query up to last 9 days, restricted by Twitter
3) Method to load data into an sqlite database or directly in a pandas dataframe for analysis

The code is currently executed daily on an aws server via a *cron task* to download the previous day tweets related to Brexit. 
The code also sends an email to report on execution. 

|

Analysing twitter data
---------------------

The second module *tweetskit.py* implements method specific to the analysis of twitter data. Method includes

1) Anomaly detection using convolution
2) Trend change detection using bayesian algorythm
3) Tweet volume forecasting using an arima model 
4) Topic modelling using latent dirichlet allocation
5) Tweet classification using bag of words and hdbscan
6) Sentiment analysis using vader sentiment analysis tools

The notebook included in the repository showcase how to use most of the methods on twitter data related to Brexit