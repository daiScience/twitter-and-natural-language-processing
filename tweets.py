# -*- coding: utf-8 -*-
'''
tweets.py
~~~~~~~~~
This module implements download and manipulation of tweets in python
This module primary use is to daily download and save tweets then notify via mail

Notable functions in this module
    retry: decorator to retry a failure-prone function, use exponential backoff
    sendmail: send an email, attach files and/or dataframe and/or running code 
    get_days_tweets: download history of tweets and save into daily pickle files
    automate: download yesterday's tweet data, save into a file and send report
'''

import tweepy   # library for accessing the Twitter API
# Note on python 3.7 https://github.com/tweepy/tweepy/issues/1017
import json     # JSON encoder and decoder
import datetime # basic date and time types
import os       # miscellaneous operating system interfaces
import inspect  # inspect live objects
import pickle   # python object serialization
import smtplib  # SMTP protocol client
import pandas   # data analysis library
import imaplib  # IMAP4 protocol client
import email    # email and MIME handling package
import sys      # system-specific parameters and functions
import time     # time access and conversions
import sqlite3  # interface for SQLite databases
from functools            import wraps         # higher-order functions and operations on callable objects
from email.mime.base      import MIMEBase      # base class for all the MIME-specific subclasses of Message
from email.mime.multipart import MIMEMultipart # base class for MIME messages that are multipart
from email.mime.text      import MIMEText      # class to create MIME objects of major type text
from email.utils          import formatdate    # date string as per RFC 2822 (ex Fri, 09 Nov 2001 01:08:47 -0000)
from email                import encoders      # encode payloads for transport through compliant mail servers
from email.utils          import COMMASPACE    # convenience, basically ', '


# twitter access detail template
CONSUMER_KEY = 'YOUR-CONSUMER-KEY'
CONSUMER_SECRET = 'YOUR-CONSUMER-SECRET'
ACCESS_TOKEN = 'YOUR-ACCESS-TOKEN'
ACCESS_SECRET = 'YOUR-ACCESS-SECRET'

# gmail access detail template
MAIL_USERNAME = 'YOUR-EMAIL-USERNAME'
MAIL_PASSWORD = 'YOUR-EMAIL-PASSWORD'

# sqlite path to database 
SQLITE_DB_PATH = os.path.join(os.path.dirname(__file__), 'tweets.sqlite3')

def load_api(parser=None, wait=False):
    ''' Load the twitter API 
    
    :parser: parser to use instead of default tweepy.models.Status objects
    :wait: bool, whether to wait if limit is reach
        Note default False to avoid disconnection while waiting, use decorator instead 
    '''
    if not parser:
        parser = tweepy.parsers.JSONParser()
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    return tweepy.API(auth, parser=parser, wait_on_rate_limit=wait)
 
def retry(ExceptionToCheck, tries=10, delay=1, backoff=2, logger=None):
    '''Retry calling the decorated function using an exponential backoff

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :ExceptionToCheck: the exception or tuple of exceptions to check
    :tries: int, number of times to try (not retry) before giving up
    :delay: int, initial delay between retries in seconds
    :param backoff: int, backoff multiplier e.g. value of 2 will double the delay
    :logger: logger to use. If None, print
    '''
    def deco_retry(f):
        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = '%s, Retrying in %d seconds...' % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else: 
                        print(msg)
                    if mdelay>0:
                        time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)
        return f_retry
    return deco_retry

@retry(tweepy.RateLimitError, delay=60*15+2, backoff=1)
def search_tweets(api, query, count=100, max_id=None, tweet_mode='extended', exclude_retweets=True):
    ''' Query the twitter api using the twitter wrapper
    
    :api: tweety api object
    :query: text to search on twitter
    :count: number of tweets to collect, api maximum is 100
    :max_id: only return statuses with an ID lower than (order than) the specified ID
    :tweet_mode: ap option, 'extended' does not truncate tweet (retweets are still truncated)
    :exclude_retweets: api query filtering, whether to exclude retweets in results
    '''
    q = query+' -filter:retweets' if exclude_retweets else query
    if max_id:    
        search = api.search(q=q, count=count, tweet_mode=tweet_mode, max_id=max_id)
    else:
        search = api.search(q=q, count=count, tweet_mode=tweet_mode)
    return search

def clean_tweets(search_results):
    ''' Create digestable output of api search results from search_tweets '''
    __list__ = ['created_at', 'id', 'id_str', 'full_text', 'truncated', 'display_text_range', 'entities'
                , 'metadata', 'source', 'in_reply_to_status_id', 'in_reply_to_status_id_str'
                , 'in_reply_to_user_id', 'in_reply_to_user_id_str', 'in_reply_to_screen_name', 'user'
                , 'geo', 'coordinates', 'place', 'contributors', 'is_quote_status', 'retweet_count'
                , 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang']                    
    dic = {}
    for i in range(len(search_results['statuses'])):
        key = search_results['statuses'][i].get('id')
        date_str = search_results['statuses'][i].get('created_at') # 'Sat Sep 22 23:59:59 +0000 2018'
        date = datetime.datetime.strptime(date_str, '%a %b %d %H:%M:%S %z %Y')
        tweet = search_results['statuses'][i].get('full_text')
        lang = search_results['statuses'][i].get('lang')
        #place = search_results['statuses'][i].get('place')
        dic[key] = dict(date=date, date_str=date_str, tweet=tweet, lang=lang)
    return dic
    
@retry(tweepy.RateLimitError, delay=60*15+2, backoff=1)
def get_max_id(api, query, date=None):
    ''' Get maximum id related to a query up until a given datetime date, default to yesterday's last id
    
    :api: tweety api object
    :query: text to search on twitter
    :date: data datetime format, default to today for yesterday last id
    '''
    if not date: 
        date = datetime.datetime.now()
    date_str = '{0}-{1:0>2}-{2:0>2}'.format(date.year, date.month, date.day)
    tweets = api.search(q=query+' -filter:retweets', count=1, until=date_str)
    created = datetime.datetime.strptime(tweets['statuses'][0]['created_at'], '%a %b %d %H:%M:%S %z %Y')
    return (tweets['statuses'][0]['id'], date, query, created)

def get_tweets(api, query, date=None, verbose=False):
    ''' Get tweets on a given date 
    
    :api: tweety api object
    :query: text to search on twitter
    :date: data datetime format, None for yesterday's tweets
    :verbose: whether to print information on each loop
    '''
    max_id, _, _, tweet_date = get_max_id(api, query, date) if date else get_max_id(api, query)
    min_time, tweets = tweet_date, {}
    gate_keeper = True
    while min_time.date()==tweet_date.date() and gate_keeper:
        start = len(tweets)
        tweets.update(clean_tweets(search_tweets(api, query, max_id=max_id)))
        max_id = min(tweets)
        min_time = tweets[max_id]['date']
        if start == len(tweets):
            gate_keeper=False
        if verbose:
            print('%s tweets downloaded...' %(len(tweets)-start))
    return {key: value for key, value in tweets.items() if value['date'].date()==tweet_date.date()}
    
def where(depth=1):
    ''' Return location where the code is running '''
    return os.path.dirname(os.path.abspath(inspect.stack()[depth][1])) if depth else None    
    
def save_tweets(tweets, query):
    ''' Write a dump file of tweet dictionary 
    
    :tweets: tweet dictionary from clean_tweets
    :query: text to search on twitter
    '''
    one_key = next(iter(tweets))
    datestamp = tweets[one_key]['date'].strftime('%Y_%m_%d')
    file_name = os.path.join(where(), datestamp+'_'+query+'.pkl')
    with open(file_name, 'wb') as f:
            pickle.dump(tweets, f, pickle.HIGHEST_PROTOCOL)
    return file_name

def load_tweets_file(file_name):
    ''' Load a dictionary of tweets from file '''
    if os.path.isfile(file_name):
        file = file_name
    elif os.path.isfile(os.path.join(where(), file_name)):
        file = os.path.join(where(), file_name)
    else:
        file = file_name
    with open(file, 'rb') as f:
        while True:
            try:
                dic = pickle.load(f)
            except EOFError:
                break
    return dic 

def sizeof_fmt(num, suffix='B'):
    ''' Human readable version of file size
        https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size '''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)
        
def list_tweets_files(dir=where()):
    ''' List the tweets files in a directory, default to directory where code is running '''
    files = [(f, os.path.getsize(os.path.join(where(), f))) for f in os.listdir(dir) if '.pkl' in f]
    files.sort(key=lambda tup: tup[0], reverse=True)
    files = files+[('total', sum(each[1] for each in files))]
    files = [(f, s, sizeof_fmt(s)) for f, s in files]
    return files

def sendmail(to=MAIL_USERNAME, username=MAIL_USERNAME, password=MAIL_PASSWORD, cc=None, subject=None
        , body=None, files=None, datas=None, attach_script=False, server='smtp.gmail.com'
        , port=465, sender='DAI SCIENCE LTD', isTls=False, isSSL=True):
    ''' Send an email, very convenient as part of automated process or to notify end of long running codes
        Using gmail, need to allow less secure apps https://support.google.com/accounts/answer/6010255?hl=en
    
    :to: required, list email recipients
    :username: required text, email username
    :password: required text, email password
    :cc: list of email copied recipients
    :subject: text for subject line of the email
    :text: body of the email
    :files: list of attachment files
    :datas: list of dataframe to attach
    :attach_script: bool, attach the running python script
    :server: email server
    :port: email server port
    :sender: email sender
    :isTls: bool, use encryption protocol 
    :isSSL: bool, use SSL protocol
    '''
    
    # create the message 
    msg = MIMEMultipart()
    msg['To'] = COMMASPACE.join([to]) if isinstance(to, str) else COMMASPACE.join(to)
    msg['From'] = sender if sender else ''
    msg['Cc'] = COMMASPACE.join([cc]) if cc and isinstance(to, str) else COMMASPACE.join(cc) if cc else ''
    msg['Subject'] = subject if subject else ''
    msg['Date'] = formatdate(localtime=True)
    attach_files = [files] if files and isinstance(files, str) else fils.copy() if files else None
    
    # setup footer and attach the python running script
    footer = 'PS This email was generated by the code "%s"' %os.path.realpath(__file__)
    if attach_script and os.path.isfile(os.path.realpath(__file__)): 
        footer+= 'attached for reference'
        if files:
            attach_files.append(os.path.realpath(__file__)) # warning!!! Modify files so needs to change
        else:
            attach_files = [os.path.realpath(__file__)]
    elif attach_script and not os.path.isfile(os.path.realpath(__file__)):
        footer+= ' Script could not be attached'

    # header of the body 
    header = 'This is an automated email. Please do not reply to this email'

    # export a csv for each dataframe
    todelete = None
    if datas:
        todelete=[]
        for d in datas:
            if isinstance(d, pandas.DataFrame):
                attach_name = '__attach'+datas.index(d)+'_sendmail.csv'
                d.to_csv(attach_name, index=False)
                attach_files.append(attach_name)
                todelete.append(attach_name)
            else:
                header+='\n\n Note an object could not be attached because not a dataframe (%s)' %d 
    
    # add attachements
    if attach_files:
        for f in attach_files:
            if os.path.isfile(f):
                part = MIMEBase('application', "octet-stream")
                part.set_payload( open(f,"rb").read() )
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(f)))
                msg.attach(part)
            else:
                header+='\n Note the file "%s" was not found and could not be attached.' %f

    # clean working directory 
    if todelete:
        for file in todelete:
            if os.path.isfile(file):  
                os.remove(file)
    
    # finalise the message 
    space = '\n\n'
    msgtext = header+space+body+space+footer if body else header+space+footer
    msg.attach(MIMEText(msgtext))

    # connect, login and send the email 
    smtp = smtplib.SMTP_SSL(server, port) if isSSL else smtplib.SMTP(server, port)
    if isTls: smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(sender, to, msg.as_string())
    smtp.quit()
    return None

def get_day_tweets(api, query, date=None, verbose=False):
    ''' Get and save tweets of the past day 
    
    :api: tweety api object
    :query: text to search on twitter
    :date: data datetime format, None for yesterday's tweets
    :verbose: whether to print number of tweets downloaded and location of file saved
    '''
    tweets = get_tweets(api, query, date) if date else get_tweets(api, query)
    file_name = save_tweets(tweets, query)
    if verbose:
        print('%s tweets downloaded in %s...'%(len(tweets), file_name))
    return file_name

def get_days_tweets(api, query, days=9, verbose=False):
    ''' Get and save tweets of the past days, 9 maximum by twitter api limitation
    
    :api: tweety api object
    :query: text to search on twitter
    :days: number of days to collect tweets from, 1 for yesterday, 2 for yesterday and day before 
    :verbose: whether to print number of tweets downloaded and location of file saved    
    '''
    file_names = []
    dates = [datetime.datetime.now() - datetime.timedelta(days=i) for i in range(days,0,-1)]
    for date in dates:
        tweets = get_tweets(api, query, date, verbose=verbose)
        file_name = save_tweets(tweets, query)
        if verbose:
            print('%s tweets downloaded in %s...' %(len(tweets), file_name))
        file_names.append(file_name)
    return file_names

def daily_tweets(api=None, query='brexit', verbose=False):
    ''' Load twitter api, download and save yesterday tweets into a pickle file 
    
    :api: tweety api object, if None result of load_api
    :query: text to search on twitter
    :verbose: whether to print information along the process
    '''
    if not api:
        api=load_api()
    return get_day_tweets(api, query, verbose=verbose)
    
def connect_sqlite(db_path=SQLITE_DB_PATH):
    ''' Connect to the tweet database given the database path '''
    return sqlite3.connect(db_path)

def create_db_sqlite(con):
    ''' Create tweets database '''
    cur = con.cursor()
    sql_delete_if_exist = 'DROP TABLE IF EXISTS tweets'
    sql_create = '''
    CREATE TABLE tweets (
        id INTEGER PRIMARY KEY
        , date DATETIME
        , date_str VARCHAR(32)
        , tweet TEXT
        , lang VARCHAR(4)
    ) '''
    cur.execute(sql_delete_if_exist)
    con.commit()
    cur.execute(sql_create)
    con.commit()
    return con
    
def load_db_tweet_dic(dic, con):
    ''' Load a dictionary of tweets into the tweets database '''
    cur = con.cursor()
    sql_insert = '''INSERT INTO tweets VALUES (?,?,?,?,?)'''
    for id in dic:
        values = [id, dic[id]['date'], dic[id]['date_str'], dic[id]['tweet'], dic[id]['lang']]
        cur.execute(sql_insert, values)
    con.commit()
    return con 

def load_db(con=None, verbose=False):
    ''' Load all tweets file where code is running into the sqlite database '''
    if not con:
        con=connect_sqlite()
    create_db_sqlite(con)
    files = list_tweets_files()
    for file in files:
        if os.path.isfile(file[0]) or os.path.isfile(os.path.join(where(), file[0])):
            tweets = load_tweets_file(file[0])
            load_db_tweet_dic(tweets, con)
            if verbose:
                print('File %s loaded, %s tweets added.'%(file, len(tweets)))
    con.commit()
    return con

def check_size_db(con):
    ''' Check the number of tweets in the sqlite database '''
    cur = con.cursor()
    sql = 'SELECT COUNT(*) FROM tweets'
    cur.execute(sql)
    return cur.fetchall()[0][0]

def to_pandas(con=None):
    ''' Convert tweet sqlite database to a pandas dataframe '''
    if not con:
        con=connect_sqlite()    
    df = pandas.read_sql_query("SELECT * FROM tweets", con)
    df['date'] = pandas.to_datetime(df['date'])
    return df

def do_report(con):
    ''' Build a basic report on files on disk, number of tweets in the sqlite database '''
    report = 'tweets db contains {:,} records.'.format(check_size_db(con))
    report+= ' Files loaded are\n  '
    report+= '\n  '.join([file[0]+' '+file[2] for file in list_tweets_files()])
    return report 

def automate(query='brexit', verbose=True):
    ''' Download yesterday's tweet data collection, save tweets into a file and send report '''
    try:
        api = load_api()
        daily_tweets(api, query=query, verbose=verbose)
        con = connect_sqlite()
        load_db(con=con, verbose=verbose)
        sendmail(subject='Daily tweet downloaded', body=do_report(con), attach_script=True)
    except:
        sendmail(subject='Errors in tweet process - Please check', attach_script=True)
    return None
    
if __name__ == '__main__':
    
    automate()
