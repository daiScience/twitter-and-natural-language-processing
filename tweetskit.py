# -*- coding: utf-8 -*-
'''
tweetskit.py
~~~~~~~~~~~~
This module implements methods to analyse the tweet database
    Anomaly detection using a convolution algorythm
    Trend change detection using bayesian algorythm
    Tweet volume forecasting using an arima model
    Unsupervised topic detection using hierarchical dirichlet
    Sentiment analysis using vader sentiment tool for social media data 
'''

import numpy      # multi-dimensional arrays and matrices
import pandas     # data analysis tools
import matplotlib # base plotting
import math       # mathematical functions
import seaborn    # statistical data visualization
import os         # miscellaneous operating system interfaces
import inspect    # inspect live objects
os.environ['MKL_THREADING_LAYER'] = 'GNU' # required for theano via pymc3 on windows python3.7
import pymc3      # bayesian statistical modeling and probabilistic machine learning 
import itertools  # functions creating iterators for efficient looping
import sklearn    # machine learning library 
import scipy      # scientific computing and technical computing
import warnings   # warning control 
import nltk       # natural language toolkit
import PIL        # python image library
import emoji      # emoji encoding in python 
import wordcloud  # little word cloud generator supporting masks
import textblob   # simplified text processing
import string     # common string operations 
import gensim     # topic modelling for humans
import hdbscan    # density based unsupervised learning for clustering
import statsmodels.api # statistics in Python
import collections     # container datatypes
import nltk.sentiment  # sentiment analysis
import sklearn.decomposition # matrix decomposition algorithms

#warnings.filterwarnings('once')

def set_plot_param():
    ''' Setting some default plot parameters around resizing figure and figure elements '''
    plot_figsize = (18.0, 10.0)
    seaborn.set(context='notebook', style='white', palette='colorblind', rc={'figure.figsize':plot_figsize})
    params = {
        'figure.figsize': plot_figsize
        , 'figure.titlesize': 20 
        , 'axes.labelsize': 16
        , 'axes.facecolor': 'white'
        , 'axes.titlepad': 15.0  
        , 'axes.titlesize': 20 
        , 'axes.labelpad': 8
        , 'axes.edgecolor': 'black'
        , 'axes.linewidth': 0.5
        , 'xtick.labelsize': 14
        , 'ytick.labelsize': 14
        , 'legend.fontsize': 14
        }
    matplotlib.rcParams.update(params)
    return None

def get_counts(data, rule='h'):
    ''' Count the number of tweets over a time series frequency 
        
    :data: pandas dataframe, result of tweet.to_pandas
    :rule: time series frequency; 'h' for hours, '10min' for 10 minutes, '2d' for 2 days etc
        http://pandas.pydata.org/pandas-docs/stable/timeseries.html#offset-aliases
    '''
    return data.set_index('date').resample(rule).id.count()

def plot_tweets(data, rule='h', rolling=1, bar=False):
    ''' Plot rolling numbers of tweets over a time series frequency 
    
    :data: pandas dataframe, result of tweet.to_pandas    
    :rule: time series frequency; 'h' for hours, '10min' for 10 minutes, '2d' for 2 days etc
    :rolling: int, rolling period to consider, default to 1 i.e. none
    :bar: whether to plot as a bar chart or a line chart
    '''
    period = get_counts(data, rule=rule).rolling(window=rolling, min_periods=1).mean()
    if bar:
        width = len(period.index.normalize().unique())/len(period.index)
        matplotlib.pyplot.bar(period.index, period, alpha=0.5, width=width)
    else:
        matplotlib.pyplot.plot(period)
    matplotlib.pyplot.title('Numbers of tweets (period=%s, rolling=%s)'%(rule,rolling))
    matplotlib.pyplot.xlabel('Period')
    matplotlib.pyplot.ylabel('Numbers of tweets')
    matplotlib.pyplot.show()
    return period 

def plot_language(data):
    ''' Plot proportion of tweets by tweet language '''
    counts = data.lang.value_counts(normalize=True)
    ax = matplotlib.pyplot.subplot(211)
    matplotlib.pyplot.bar(counts.index, counts)
    matplotlib.pyplot.xticks(rotation=45)
    matplotlib.pyplot.gca().set_title('Tweet language')
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, _: '{:.0%}'.format(x)))
    ax = matplotlib.pyplot.subplot(212)
    not_english = counts.index!='en'
    matplotlib.pyplot.bar(counts.index[not_english], counts[not_english])
    matplotlib.pyplot.xticks(rotation=45)
    matplotlib.pyplot.gca().set_title('Tweet language excluding english (%s%%)'%round(float(counts[~not_english])*100,1))
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, _: '{:.1%}'.format(x)))
    matplotlib.pyplot.tight_layout()
    matplotlib.pyplot.show()
    return counts

def average_period(counts, period):
    ''' Computes moving average of counts over a period length using convolution 
    
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :period: int, rolling period to compute convolution on 
    '''
    return numpy.convolve(counts, numpy.ones(period)/period, mode='same')
    
def anomaly(counts, period, stationary=False, threshold=2):
    ''' Find points deviating from average by a threshold of the standard deviation 
    
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :period: int, rolling period for moving average using convolution
    :stationary: whether to consider standard deviation or not (versus rolling standard deviation)
    :threshold: value of standard deviation above which to be considered an anomaly
    '''
    average = average_period(counts, period)
    residuals = counts - average
    deviation = numpy.std(residuals)
    variation = pandas.Series(residuals).rolling(window=period).std().values
    variation[0:period-1] = variation[period-1]
    standard = [deviation]*len(variation) if stationary else variation
    return [(counts.index[i], counts[i]) for i in range(len(counts)) if abs(counts[i])>average[i]+threshold*standard[i]]
    
def plot_anomaly(data, rule='h', period=24, stationary=False, threshold=2, bar=False):
    ''' Plot and return anomalies deviating from average by a threshold of standard deviation 
    
    :data: pandas dataframe, result of tweet.to_pandas
    :rule: time series frequency; 'h' for hours, '10min' for 10 minutes etc
    :period: int, rolling period for moving average using convolution
    :stationary: whether to consider standard deviation or not (versus rolling standard deviation)
    :threshold: value of standard deviation above which to be considered an anomaly    
    :bar: whether to plot as a bar chart or a line chart
    '''
    counts = get_counts(data, rule)
    if bar:
        width = len(counts.index.normalize().unique())/len(counts.index)
        matplotlib.pyplot.bar(counts.index, counts, alpha=0.5, width=width, label='Number of tweets')
    else:
        matplotlib.pyplot.plot(counts, label='Number of tweets')
    average = average_period(counts, period)
    label = 'Average over %s periods'%period
    matplotlib.pyplot.plot(counts.index, average, linestyle='--', color='darkgrey', label=label)
    anomalies = anomaly(counts, period, stationary=stationary, threshold=threshold)
    x, y = [coor[0] for coor in anomalies], [coor[1] for coor in anomalies]
    matplotlib.pyplot.plot(x, y, 'r*', markersize=15, label='anomalies')
    matplotlib.pyplot.title('Numbers of tweets (period=%s)'%rule)
    matplotlib.pyplot.legend()
    matplotlib.pyplot.show()
    return anomalies
    
def get_trace(counts, summary=True, draws=5000, tune=1000, chains=10, cores=4, random_state=None):
    ''' Collect trace of a markov chain monte carlo, i.e. samples of posterior distributions of lambda1, lambda2 and tau
        Assumption is count follows a poisson distribution with parameter lambda1 and at event tau lambda1 changes to lambda2
    
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :summary: whether to print summary of the trace or not 
    :draw: number of samples to draw from the model
    :tune: number of iterations to tune
    :chains: number of chains to sample, increase to improve convergence statistics and reveal multiple modes in posterior
    :cores: number of chains to run in parallel, higher than 1 crashes on windows as most multiprocesses
    :step: step function or collection of functions, pymc3.Metropolis for metropolis–hastings algorithm
    :random_state: random seed for markov chain monte carlo
    '''
    with pymc3.Model() as model:
        param = dict(draws=draws, chains=chains, cores=cores, tune=tune)
        if random_state:
            param['random_seed'] = random_state
        alpha = 1.0/counts.mean()
        lambda_1 = pymc3.Exponential('lambda_1', alpha)
        lambda_2 = pymc3.Exponential('lambda_2', alpha)
        tau = pymc3.DiscreteUniform('tau', lower=0, upper=len(counts)-1)
        idx = numpy.arange(len(counts))
        lambda_ = pymc3.math.switch(tau > idx, lambda_1, lambda_2)
        observation = pymc3.Poisson('obs', lambda_, observed=counts)
        trace = pymc3.sample(**param, step=pymc3.Metropolis())
        if summary:
            print(pymc3.summary(trace))
        return trace
        
def plot_trace(trace, counts, bins=100, colors=['#A60628', '#7A68A6', '#467821'], alpha=0.7):
    ''' Plot posterior distributions of lambda1, lambda2 and tau from trace of a Markov Chain Monte Carlo
    
    :trace: output from get_trace, result of pymc3.sample
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :bins: matplotlib histogram bin parameter
    :colors: list of colors for the 3 histograms
    :alpha: alpha transparency of the plot colors
    '''    
    xmin = min(numpy.minimum(trace['lambda_1'], trace['lambda_2']))
    xmax = max(numpy.maximum(trace['lambda_1'], trace['lambda_2']))
    param = dict(histtype='stepfilled', alpha=alpha, bins=bins, density=True)
    ax = matplotlib.pyplot.subplot(311)
    matplotlib.pyplot.hist(trace['lambda_1'], color=colors[0], **param)
    matplotlib.pyplot.gca().set_title('posterior of $\lambda_1$')
    matplotlib.pyplot.xlabel('$\lambda_1$ value')
    matplotlib.pyplot.xlim([xmin, xmax])
    
    ax = matplotlib.pyplot.subplot(312)
    matplotlib.pyplot.hist(trace['lambda_2'], color=colors[1], **param)
    matplotlib.pyplot.gca().set_title('posterior of $\lambda_2$')
    matplotlib.pyplot.xlabel('$\lambda_2$ value')
    matplotlib.pyplot.xlim([xmin, xmax])
    
    matplotlib.pyplot.subplot(313)
    w = 1.0 / trace['tau'].shape[0] * numpy.ones_like(trace['tau'])
    matplotlib.pyplot.hist(trace['tau'], alpha=alpha, weights=w, width=1, color=colors[2])
    matplotlib.pyplot.gca().set_title(r'posterior of $\tau$')
    labels = [(i, period.date()) for i, period in enumerate(counts.index) if period.hour==12]
    matplotlib.pyplot.xticks(*zip(*labels), rotation=45)
    matplotlib.pyplot.ylim([0, 1.2])
    matplotlib.pyplot.xlabel(r'$\tau$')
    matplotlib.pyplot.tight_layout()
    matplotlib.pyplot.show()
    return trace

def get_expected(trace, counts):
    ''' Get expected number of tweets from a Markov Chain Monte Carlo '''
    N = trace['tau'].shape[0]
    expected = numpy.zeros(len(counts))
    for each in range(0, len(counts)):
        ix = each < trace['tau']
        expected[each] = (trace['lambda_1'][ix].sum()+trace['lambda_2'][~ix].sum())/N
    return expected 
    
def plot_expected(trace, counts, bar=False):
    ''' Plot expected number of tweets from a Markov Chain Monte Carlo versus observed number 
    
    :trace: output from get_trace, result of pymc3.sample
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :bar: whether to plot counts as a bar chart or a line chart
    '''
    expected = get_expected(trace, counts)
    if bar:
        width = len(counts.index.normalize().unique())/len(counts.index)
        matplotlib.pyplot.bar(counts.index, counts, alpha=0.5, width=width, label='Observed number of tweets')
    else:
        matplotlib.pyplot.plot(counts,label='Observed number of tweets')
    matplotlib.pyplot.plot(counts.index, expected, color='#E24A33', label='Expected number of tweets')
    matplotlib.pyplot.xlabel('Period')
    matplotlib.pyplot.ylabel('Number of tweets')
    matplotlib.pyplot.title('Expected versus observed number of tweets')
    matplotlib.pyplot.show()
    return trace

def decompose(data, rule='h', rolling=1, model='additive', freq=None):
    ''' Seasonal decomposition of a time series using moving averages
    
    :data: pandas dataframe, result of tweet.to_pandas
    :rule: time series frequency; 'h' for hours, 'd' for d etc
    :rolling: int, rolling period to consider, default to 1 i.e. none
    :model: type of seasonal component, additive or multiplicative
    :freq: frequency of the time series, unnecessary if rule is 'h' or 'd'
    '''
    counts = get_counts(data, rule=rule)
    if freq:
        decomposition = statsmodels.api.tsa.seasonal_decompose(counts, model=model, freq=freq)
    else:
        decomposition = statsmodels.api.tsa.seasonal_decompose(counts, model=model)
    fig = decomposition.plot()
    matplotlib.pyplot.show()
    return counts

def get_stats(counts, order, seasonal_order, training_ratio=0.7):
    ''' Get seasonal arima model statistics for a given order and seasonal order. Collected statisticss are 
        in sample akaike information and bayes information criterion, out of sample mean squared, mean absolute and mean absolute scaled errors

    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :order: tuple representing the order of arima model 
    :seasonal_order: tuple representing the seasonal order of arima model
    :training_ratio: percentage representing size of training for cross validation
    '''
    try:
        param = dict(order=order, seasonal_order=seasonal_order, enforce_stationarity=False, enforce_invertibility=False)
        train_size = int(len(counts) * training_ratio)
        train, test = counts[0:train_size], counts[train_size:]
        model = statsmodels.api.tsa.statespace.SARIMAX(train, **param)
        model_fit = model.fit()
        forecast = model_fit.forecast(len(counts)-train_size)
        mae = round(sklearn.metrics.mean_absolute_error(test, forecast), 4)
        rmse = round(math.sqrt(sklearn.metrics.mean_squared_error(test, forecast)), 4)
        smape = round(1/len(test)*numpy.sum(2*numpy.abs(forecast-test)/(numpy.abs(test)+numpy.abs(forecast))),4)
        return (order, seasonal_order, (round(model_fit.aic, 4), round(model_fit.bic, 4), mae, rmse, smape))
    except:
        return (order, seasonal_order, (float('inf'), float('inf'), float('inf'), float('inf'), float('inf')))

def create_combinations(p, d, q, m, sp, sd, sq):
    ''' Create all combinations of orders and seasonal orders of an arima model for a grid search
    
    :p: range for possible numbers of time lags for the order of the autoregressive model
    :d: range for possible degree of differencing 
    :q: range for possible orders of the moving-average model
    :m: number of periods in each season
    :sp: range for possible numbers of time lags of the seasonal order
    :sd: range for possible degree of differencing of the seasonal order
    :sq: range for possible orders of the moving-average model of the seasonal order
    '''
    return (list(itertools.product(p, d, q)), [(x[0], x[1], x[2], m) for x in list(itertools.product(sp, sd, sq))])
        
def grid_search(counts, orders, seasonal_orders, verbose=True):
    ''' Grid search for arima model, compute seasonal arima statistics for two lists of orders and seasonal orders
    
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :orders: list of tuple representing the orders of arima model 
    :seasonal_orders: list of tuple representing the seasonal orders of arima model
    '''
    if verbose:
        combinations = len(orders)*len(seasonal_orders)
        print('Grid searching %s combinations'%combinations)
    return [get_stats(counts, o, so) for o in orders for so in seasonal_orders]
        
def score_grid(grid_results, weights=None, top=10):
    ''' Score each order and seasonal order combination statistics given a weight for each statistics
    
    :grid_results: list of tuples, results of grid_search
    :weights: list of weights for each statistics, default to None for equal weights 
    :top: return n top combinations, None for all 
    '''
    nstats = len(grid_results[0][2])
    if not weights:
        weights = [1/nstats]*nstats
    score = numpy.zeros(len(grid_results))
    for i in range(nstats):
        score+= scipy.stats.rankdata([each[2][i] for each in grid_results], method='max')*weights[i]
    res = sorted([(round(score[i], 1),)+grid_results[i] for i in range(len(grid_results))], key=lambda x: x[0])
    if top:
        for each in res[:top]:
            print(each)
        return res[:top]
    return res
    
def get_arima(counts, order, seasonal_order, summary=True, ndays=3):
    ''' Get arima model for an order and seasonal order and summarize 
    
    :counts: numpy array, aggregation of tweet.to_pandas to a time series frequency using get_counts
    :orders: list of tuple representing the orders of arima model 
    :seasonal_orders: list of tuple representing the seasonal orders of arima model    
    :summary: whether to print summary of the model, plot diagnostics and observed versus forecast for past 2 seaonsal periods
    :ndays: number of days to compare forecast to actuals
    ''' 
    param = dict(order=order, seasonal_order=seasonal_order, enforce_stationarity=False, enforce_invertibility=False)
    model = statsmodels.api.tsa.statespace.SARIMAX(counts, **param).fit()
    if summary:
        print(model.summary().tables[1])
        model.plot_diagnostics()
        matplotlib.pyplot.show()
        start = counts.index[-ndays*seasonal_order[3]]
        prediction = model.get_prediction(start=start, dynamic=False)
        confidence = prediction.conf_int(alpha = 0.05)
        actuals = counts[start:]
        ax1 = counts.plot(label='Number of tweets')
        prediction.predicted_mean.plot(ax=ax1, label='One-step ahead forecast', linestyle='--', color='r', alpha=0.7)
        ax1.fill_between(confidence.index, confidence.iloc[:, 0], confidence.iloc[:, 1], color='k', alpha=.2, label='95% confidence')
        ax1.set_xlabel('Period')
        ax1.set_ylabel('Number of tweets')
        ax1.set_title('Comparison actual number of tweets versus forecast') 
        matplotlib.pyplot.legend()
        matplotlib.pyplot.show()
    return model

def add_word_count(data, column_in='tweet', column_out='word_count'):
    ''' Add a tweet word count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len(str(x).split(' ')))
    return None
    
def add_char_count(data, column_in='tweet', column_out='char_count'):
    ''' Add a tweet character count column in tweet dataframe '''
    data[column_out] = data['tweet'].str.len()
    return None 

def add_avg_word(data, column_in='tweet', column_out='avg_word'):
    ''' Add a tweet average word length column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: sum(len(word) for word in x.split())/len(x.split()))
    return None 

def add_stopword_count(data, column_in='tweet', column_out='stopword_count'):
    ''' Add a tweet stopword count column in tweet dataframe '''
    stopwords = nltk.corpus.stopwords.words('english')
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x in stopwords]))
    return None

def add_hashtag_count(data, column_in='tweet', column_out='hashtag_count'):
    ''' Add a tweet hashtag count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x.startswith('#')]))
    return None 

def add_link_count(data, column_in='tweet', column_out='link_count'):
    ''' Add a tweet link count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x.startswith('http')]))
    return None 

def add_user_count(data, column_in='tweet', column_out='user_count'):
    ''' Add a tweet user mention count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x.startswith('@')]))
    return None 
    
def add_numeric_count(data, column_in='tweet', column_out='numeric_count'):
    ''' Add a tweet numeric count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x.isdigit()]))
    return None 
    
def add_upper_count(data, column_in='tweet', column_out='upper_count'):
    ''' Add a tweet upper word count column in tweet dataframe '''
    data[column_out] = data[column_in].apply(lambda x: len([x for x in x.split() if x.isupper()]))
    return None    

def add_basic_features(data, column_in='tweet', copy=True, verbose=True):
    ''' Add features to the tweet dataframe '''
    feature_data = data.copy() if copy else data
    todo = [ 
        add_word_count, add_char_count, add_avg_word, add_stopword_count
        , add_hashtag_count, add_link_count, add_user_count
        , add_numeric_count, add_upper_count
        ]
    for each in todo:
        if verbose:
            print ('executing %s on %s'%(each, column_in))
        each(feature_data, column_in=column_in)
    return feature_data

def remove_amp(data, column_in='tweet', column_out='no_amp'):
    ''' Remove &amp; from tweets, twitter converts & to &amp: '''
    data[column_out] = data[column_in].str.replace('&amp;', '')
    return None

def remove_links(data, column_in='tweet', column_out='no_links'):
    ''' Remove links from tweets '''
    data[column_out] = data[column_in].apply(lambda x: ' '.join([x for x in x.split() if not x.startswith('http')]))
    return None

def remove_users(data, column_in='tweet', column_out='no_links'):
    ''' Remove users from tweets '''
    data[column_out] = data[column_in].apply(lambda x: ' '.join([x for x in x.split() if not x.startswith('@')]))
    return None    

def lower_case(data, column_in='tweet', column_out='lower_tweet'):
    ''' Apply lower case to our tweets '''
    data[column_out] = data[column_in].apply(lambda x: ' '.join(x.lower() for x in x.split()))
    return None 
        
def remove_punctuation(data, column_in='tweet', column_out='no_punctuation'):
    ''' Remove punctuation from tweets '''
    data[column_out] = data[column_in].str.replace('[^\w\s]', '')
    return None 

def remove_stopword(data, column_in='tweet', column_out='no_stopwords'):
    ''' Remove stopwords from tweets '''
    stopwords = nltk.corpus.stopwords.words('english')
    data[column_out] = data[column_in].apply(lambda x: ' '.join(x for x in x.split() if x not in stopwords))
    return None 

def lemmatize(data, column_in='tweet', column_out='lemmatized'):
    ''' Lemmatize the tweet words, i.e. extract the canonical form of each word '''
    data[column_out] = data[column_in].apply(lambda x: " ".join([textblob.Word(word).lemmatize() for word in x.split()]))
    return None
    
def tokenize(data, column_in='tweet', column_out='tokenize'):
    ''' Separate elements into words or group of words '''
    data[column_out] = data[column_in].apply(lambda x:  textblob.TextBlob(x).words)
    return None 
    
def clean_tweets(data, column_in='tweet', copy=True, verbose=True):
    ''' Clean and pre-process the tweet '''
    cleaned_data = data.copy() if copy else data
    todo = [remove_amp, remove_users, remove_links
        , lower_case, remove_punctuation, remove_stopword
        #, lemmatize, tokenize
        ]
    for each in todo:
        if verbose:
            print ('executing %s on %s'%(each, column_in))            
        each(cleaned_data, column_in=column_in, column_out=column_in)
    return cleaned_data

def where(depth=1):
    ''' Return location where the code is running '''
    return os.path.dirname(os.path.abspath(inspect.stack()[depth][1])) if depth else None    

def plot_wordcloud(textdata, color='white', figsize=(15, 15), mask=r'twitter_mask.png'):
    ''' Plot a word cloud from text 
    
    :textdata: string, contais the text data to create the word cloud from 
    :color: back ground color, default white
    :figsize: size of the figure plot
    :mask: use a mask to plot the word cloud 
    '''
    if mask is not None and os.path.isfile(mask):
        twitter_mask = numpy.array(PIL.Image.open(mask))
    elif mask is not None and os.path.isfile(os.path.join(where(), mask)):
        twitter_mask = numpy.array(PIL.Image.open(os.path.join(where(), mask)))
    else: 
        twitter_mask = None
    if twitter_mask is not None:
        param = dict(background_color=color, mask=twitter_mask, contour_width=2
            , contour_color='steelblue', width=2500, height=2000)
        cloud = wordcloud.WordCloud(**param).generate(textdata)
        matplotlib.pyplot.figure(1, figsize)
        matplotlib.pyplot.imshow(cloud)
    else:
        param = dict(background_color=color, width=2500, height=2000)
        cloud = wordcloud.WordCloud(**param).generate(textdata)
        matplotlib.pyplot.figure(1, figsize)
        matplotlib.pyplot.imshow(cloud)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.show()
    return cloud

def get_texts(data, exclude_list=None):
    ''' Faster implementation of simple text processing on tweets 
    
    :data: pandas dataframe, result of tweet.to_pandas
    :exclude_list: list of words to ignore on top of english stopwords, emojis, links, users and hashtags
    '''
    word_ignore = nltk.corpus.stopwords.words('english')+list(emoji.UNICODE_EMOJI)+['&amp;']
    word_ignore = word_ignore+exclude_list if exclude_list else word_ignore
    start_ignore=('http','@','#')
    lmtzr=nltk.stem.WordNetLemmatizer()
    rm_punct = lambda x: x.lower().translate(str.maketrans('', '', string.punctuation))
    texts = [[rm_punct(lmtzr.lemmatize(w)) for w in each.split()
                       if rm_punct(lmtzr.lemmatize(w)) not in word_ignore 
                       and not w.startswith(start_ignore)]
             for each in data.tweet]
    return texts

def get_tfidf_scores(texts, ngram_range=(1,3), verbose=True):
    ''' Compute the Term Frequency Inverse Data Frequency score 
        tfidf is a stat to reflect how important a word is to a document
        Note ngrams will be separated by space, not underscore
        
    :texts: list of list containing words from tweets, result of get_texts
    :ngram_range: range of ngrams to include
    :verbose: boolean, print information about most important words and distribuction of score
    '''
    param = dict(lowercase=True, ngram_range=ngram_range, stop_words=None)
    vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(**param)
    response = vectorizer.fit_transform([' '.join(text) for text in texts])
    dictionary = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
    scores = collections.OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))
    if verbose:
        pcts = [0.001, 0.005, 0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 0.999]
        pcts_value = numpy.percentile([scores[key] for key in scores], pcts)
        print([(pcts[i], pcts_value[i]) for i in range(len(pcts))], end='\n\n')
        for key in list(scores.keys())[:20]:
            print(key, scores[key])
    return scores

def add_ngram(texts, ngram=3):
    ''' Add Ngram to a list of list of words
    
    :texts: list of list containing words from tweets, result of get_texts
    :ngram: iterative level of ngram to add, 2 for bigram, 3 for bigram and trigram etc
    '''
    new_texts = texts.copy()
    for i in range(1, ngram+1):
        nphrases = gensim.models.phrases.Phrases(new_texts, delimiter=b' ')
        ngram = gensim.models.phrases.Phraser(nphrases) 
        new_texts = [ngram[text] for text in new_texts]
    return new_texts

def get_corpus(texts):
    ''' Helper function to compute corpus and dictionary from texts '''
    dictionary = gensim.corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    return (corpus, dictionary)
    
def get_coherence(n_topic, corpus, dictionary, texts):
    ''' Get coherence for a number of topic '''
    model = gensim.models.LdaMulticore(corpus, id2word=dictionary, num_topics=n_topic, workers=3)
    coherence = gensim.models.CoherenceModel(model=model, texts=texts, dictionary=dictionary).get_coherence()
    return (n_topic, coherence)

def get_coherence_range(texts, start=2, end=10, by=1):
    ''' Get coherence for a range of number of topic '''
    corpus, dictionary = get_corpus(texts)
    tfidf = gensim.models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    results = []
    for i in range(start, end+1, by):
        results.append(get_coherence(i, corpus, dictionary, texts))
    return results

def plot_coherence():
    ''' Plot coherence score over number of topics '''
    pass # coherence is disregarded for establishing optimal number of topic

def get_lda(corpus, dictionary, num_topics, figsize=(18,18), num_words=25, verbose=True, random_state=None):
    ''' Helper function to get latent dirichlet allocation model 
    
    :corpus: collection of documents
    :dictionary: associated corpus dictionary 
    :num_topic: number of topic to model
    :figsize: size of the correlation matrix of words
    :num_words: number of words to extract from each topic, for plot of correlation only
    :random_state: for reproducibility
    '''
    param = dict(id2word=dictionary, num_topics=num_topics, workers=3, minimum_probability=0)
    if random_state:
        param['random_state'] = random_state
    lda = gensim.models.LdaMulticore(corpus, **param)
    data_lda = {i: collections.OrderedDict(lda.show_topic(i,num_words)) for i in range(num_topics)}
    df_lda = pandas.DataFrame(data_lda).fillna(0).T
    g=seaborn.clustermap(df_lda.corr(), center=0, cmap="RdBu", metric='cosine', linewidths=.75, figsize=figsize)
    matplotlib.pyplot.setp(g.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    matplotlib.pyplot.show()
    return lda 
    
def get_hdp(corpus, dictionary, max_topic=100, xlines=range(5,35,5), non_param_ntopic=500, random_state=None):
    ''' Helper function to get hierarchical dirichlet process and estimate number of topics in a corpus
    
    :corpus: collection of documents
    :dictionary: associated corpus dictionary
    :max_topic: maximum number of topic to plot
    :xlines: range to draw vertical line to help find optimal level
    :non_param_ntopic: number of topic to model. Keep high for a non parametric way to estimate number of topic
    :random_state: for reproducibility
    '''
    param = dict(T=non_param_ntopic)
    if random_state:
        param['random_state'] = random_state
    hdp_model = gensim.models.HdpModel(corpus, dictionary, T=non_param_ntopic)
    alpha_total = hdp_model.hdp_to_lda()[0]
    alpha = alpha_total if max_topic>non_param_ntopic else alpha_total[:max_topic]
    fig, ax1 = matplotlib.pyplot.subplots()
    x = range(len(alpha))
    plot1, = ax1.plot(alpha, label='alpha (left axis)')
    ax1.set_ylabel('alpha level')
    ax1.set_xlabel('number of topics')
    ax2 = ax1.twinx()
    plot2, = ax2.plot(x, numpy.cumsum(alpha), '--', label='Cumulative alpha (right axis)')
    ax2.set_ylabel('cumulative alpha level')
    if xlines:
        for x in xlines:
            matplotlib.pyplot.axvline(x=x, color='lightgrey', linestyle=':')
    matplotlib.pyplot.title('Alpha contribution by number of topics')
    matplotlib.pyplot.legend(handles=[plot1, plot2], loc='upper center')
    fig.tight_layout()
    matplotlib.pyplot.show()
    return hdp_model
    
def add_topics(data, model, texts, corpus, copy=True):
    ''' Add topic to the tweet data 
    
    :data: original dataframe with tweet data
    :model: latent dirichlet allocation model, from get_lda
    :texts: texts data for each tweet, from get_text
    :corpus: corpus data, from get_corpus
    :copy: whether to return a copy of the dataframe or the original
    '''
    new_data = data.copy() if copy else data
    topic_scores = [[each[1] for each in model[corpus[i]]] for i in range(len(texts))]
    n_topic = len(topic_scores[0])
    score_names = ['Topic'+str(i) for i in range(n_topic)]
    topic = pandas.DataFrame(topic_scores, columns=score_names)
    top_topic = numpy.argmax(topic.values, axis=1)
    topic['top_topic'] = top_topic
    topic['top_prop'] = [max(score) for score in topic_scores]
    topic_word = [' '.join([each[0] for each in model.show_topic(i, 10)]) for i in range(n_topic)]
    topic['texts'] = [' '.join(text) for text in texts]
    topic['keyword'] = topic['top_topic'].apply(lambda x: topic_word[x])
    return pandas.concat([new_data, topic], axis=1)

def describe_topic(topic):
    ''' Helper function to describe topic data from add_topic '''
    # histogram of top topic contribution and frequecy of topic in tweets
    ax = matplotlib.pyplot.subplot(211)
    matplotlib.pyplot.hist(topic.top_prop, bins=100, alpha=0.7)
    matplotlib.pyplot.title('Histogram of top topic contribution to tweet')
    ax = matplotlib.pyplot.subplot(212)
    prop_topic = topic['top_topic'].value_counts()
    matplotlib.pyplot.bar(prop_topic.index, prop_topic, alpha=0.7)
    matplotlib.pyplot.xticks(numpy.arange(len(prop_topic.index)))
    matplotlib.pyplot.xlabel('Topic number')
    matplotlib.pyplot.title('Distribution of topic')
    matplotlib.pyplot.tight_layout()
    matplotlib.pyplot.show()
    # most representative tweet for each topic 
    idx = topic.groupby(['top_topic'])['top_prop'].transform(max) == topic['top_prop']
    tops = topic[['id', 'date', 'top_topic', 'top_prop', 'tweet']][idx].sort_values(by='top_topic')
    print('--------------- Most representative tweet for each topic ---------------', end='\n\n')
    for i in range(len(tops.tweet)):
        print('Topic %s (id %s, %s)'%(tops.top_topic.values[i], tops.id.values[i], tops.date.values[i]))
        print(tops.tweet.values[i], end='\n\n\n')
    # segregation of topics using a singular value decomposition
    ntopic = len(set(topic.top_topic.values))
    topics = ['Topic'+str(i) for i in range(ntopic)]
    colors = seaborn.color_palette('colorblind', ntopic)
    scaled = sklearn.preprocessing.StandardScaler().fit_transform(topic[topics])
    svd = sklearn.decomposition.TruncatedSVD()
    svd_out = svd.fit_transform(scaled)
    var = int(numpy.sum(svd.explained_variance_ratio_)*100)
    x, y = svd_out[:, 0], svd_out[:, 1]
    fig, ax1 = matplotlib.pyplot.subplots()
    handles = []
    for i in range(ntopic):
        idx = topic.top_topic==i
        x, y = svd_out[idx, 0], svd_out[idx, 1]
        f = matplotlib.pyplot.scatter(x, y, c=colors[i], label='Topic %s'%i)
        handles.append(f)
    matplotlib.pyplot.legend()
    matplotlib.pyplot.title('Segregation of topic number (SVD var explained %s%%)'%var)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.show()
    return None
    
def get_cluster(texts, vectorizer=None, svd_dim=10, min_size=10000, metric='euclidean'):
    ''' Perform vectorisation (bow), dimension reduction (svd) and clustering of texts (hdbscan)
    
    :texts: list of list containing words from tweets, result of get_texts
    :vectorizer: callable function, example CountVectorizer for bag of words from sklearn or another, eg TfidfVectorizer
    :svd_dim: number of dimension for dimension reduction algorythm (singular value decomposition)
    :min_size: minimum number of observattion to consider a cluster for hdbscan
    :metric: distance metric to create the cluster for hdbscan
    '''
    if not vectorizer:
        vectorizer = sklearn.feature_extraction.text.CountVectorizer() #TfidfVectorizer()
    vector = vectorizer.fit_transform([' '.join(text) for text in texts])
    svd = sklearn.decomposition.TruncatedSVD(n_components=10)
    svd_out = svd.fit_transform(vector)
    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_size, metric=metric, prediction_data=True)
    cluster = clusterer.fit(svd_out)
    # ncluster = len(set(clusterer.labels_))
    # colors = seaborn.color_palette('colorblind', ncluster)
    # fig, ax1 = matplotlib.pyplot.subplots()
    # handles = []
    # for i in range(ncluster-1):
        # idx = clusterer.labels_==i
        # x, y = svd_out[idx, 0], svd_out[idx, 1]
        # f = matplotlib.pyplot.scatter(x, y, c=colors[i], label='Cluster %s'%i)
        # handles.append(f)
    # idx = clusterer.labels_==-1
    # x, y = svd_out[idx, 0], svd_out[idx, 1]
    # f = matplotlib.pyplot.scatter(x, y, c='lightgrey', label='Outliers')
    # handles.append(f)
    # matplotlib.pyplot.legend()
    # matplotlib.pyplot.title('Segregation of cluster on first two dimension of SVD')
    # matplotlib.pyplot.axis('off')
    # matplotlib.pyplot.show()
    return (cluster, clusterer)

def add_cluster(data, cluster, clusterer, texts, verbose=True, copy=True):
    ''' Add cluster information back to original data
    
    :data: original dataframe with tweet data
    :cluster: hdbscan cluster data, result of get_cluster
    :texts: texts data for each tweet, from get_text
    :verbose: plot basic descrition of clusters
    :copy: whether to return a copy of the dataframe or the original 
    '''
    new_data = data.copy() if copy else data 
    new_data['texts'] = texts
    new_data['cluster'] = clusterer.labels_
    new_data['probabilities'] = clusterer.probabilities_
    if verbose:
        # number of cluster and frequency plus probablity distribution
        ax = matplotlib.pyplot.subplot(211)
        unique, counts = numpy.unique(clusterer.labels_, return_counts=True)
        matplotlib.pyplot.bar(unique[1:], counts[1:], alpha=0.7)
        outliers = int(counts[0]/numpy.sum(counts)*100)
        matplotlib.pyplot.title('Distribution of cluster (%s%%=-1, not shown)'%outliers)
        matplotlib.pyplot.xlabel('Cluster number')
        matplotlib.pyplot.ylabel('Number of tweet')
        ax = matplotlib.pyplot.subplot(212)
        proba = clusterer.probabilities_[clusterer.labels_>0]
        matplotlib.pyplot.hist(proba, alpha=0.7, bins=100)
        matplotlib.pyplot.title('Distribution of probability of cluster (%s%%=-1, not shown)'%outliers)
        matplotlib.pyplot.xlabel('Cluster probability')
        matplotlib.pyplot.tight_layout()
        matplotlib.pyplot.show()
        # print('--------------- Most representative tweet for each cluster ---------------', end='\n\n')
        # idx1 = new_data.groupby(['cluster'])['probabilities'].transform(max) == new_data['probabilities']
        # idx2 = new_data['cluster']==-1
        # tops = new_data[['id', 'date', 'cluster', 'probabilities', 'tweet']][(idx1) & (~idx2)].sort_values(by='cluster')
        # for i in range(len(tops.tweet)):
            # if tops.cluster.values[i]==-1:
                # continue
            # print('Cluster %s (id %s, %s)'%(tops.cluster.values[i], tops.id.values[i], tops.date.values[i]))
            # print(tops.tweet.values[i], end='\n\n\n')
    return new_data

def add_sentiment(data, column_in='tweet', column_out=['neg', 'neu', 'pos', 'compound'], verbose=True):
    ''' Add vader tweet sentiment to the tweet dataframe 
    
    :data: original dataframe with tweet data
    :column_in: column containing text in data 
    :column_out: name of the output columns (negative, neutral and positive scores as well as compound)
    :verbose: quick analysis of the results
    '''
    analyzer = nltk.sentiment.vader.SentimentIntensityAnalyzer()
    data[column_out] = data[column_in].apply(lambda x: pandas.Series(analyzer.polarity_scores(x)))
    if verbose:
        noscore = int(data[data['compound']!=0].shape[0]/len(data.index)*100)
        matplotlib.pyplot.hist(data[data['compound']!=0]['compound'], bins=100, alpha=0.7)
        matplotlib.pyplot.xlabel('Sentiment score (-1 negative to 1 positive)')
        matplotlib.pyplot.ylabel('Number of tweets')
        matplotlib.pyplot.title('Distribution of sentiment score (null score removed, %s%%)'%noscore)
        matplotlib.pyplot.show()
        print('--------------- Most Negative tweets ---------------', end='\n\n')
        neg = data.sort_values(by='compound', ascending=True)[:10]
        for i in range(len(neg.index)):
            print('Score %s (id %s, %s)'%(neg['compound'].values[i], neg['id'].values[i], neg['date'].values[i]))
            print(neg.tweet.values[i], end='\n\n\n')
        print('--------------- Most Positive tweets ---------------', end='\n\n')
        pos = data.sort_values(by='compound', ascending=False)[:10]
        for i in range(len(pos.index)):
            print('Score %s (id %s, %s)'%(pos['compound'].values[i], pos['id'].values[i], pos['date'].values[i]))
            print(pos.tweet.values[i], end='\n\n\n')
    return None
    
if __name__ == '__main__':
    
    pass
    